const config = require("config");
const mongoose = require("mongoose");
const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);
const express = require("express");
const app = express();
const customers = require("./routes/customers");
const users = require("./routes/users");
const auth = require("./routes/auth");
const authMiddleware = require("./middleware/auth");

if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwtPrivateKey is not defined");
  process.exit(1);
}

mongoose
  .connect("mongodb://localhost/CRUD")
  .then(() => console.log("Connected to MOngoDB...."))
  .catch(() => console.error("Could not connect to MOngoDB...."));

app.use(express.json());
// Applied auth middleware for authentication
// app.use(function (req, res, next) {
//   authMiddleware(req, res, next);
// }); 

app.use("/api/customers", customers);

app.use("/api/users", users);

// Generate custom tokens - not using firebase authentiction in it 
app.use("/api/auth", auth);


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
