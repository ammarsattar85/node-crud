const auth = require("../middleware/auth");
const bcrypt = require("bcrypt");
const { User, validate } = require("../models/user");
const express = require("express");
const _ = require("lodash");
const router = express.Router();

//We can add authentication or authorization on the endpoint
router.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select(
    "-sensitiveInformation"
  );
  res.send(user);
});

router.post("/", async (req, res) => {
  //Validating schema
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });

  if (user) return res.status(400).send("User already registered");

  user = new User(
    _.pick(req.body, ["name", "email", "sensitiveInformation"])
    //   {
    //   name: req.body.name,
    //   email: req.body.email,
    //   sensitiveInformation: req.body.sensitiveInformation,
    // }
  );

  //Encrypt sensitive information
  const salt = await bcrypt.genSalt(10);
  user.sensitiveInformation = await bcrypt.hash(
    user.sensitiveInformation,
    salt
  );

  user = await user.save();

  //Creating and setting a token
  const token = user.generateAuthToken();
  res.header("x-auth-token", token).send(_.pick(user, ["id", "name", "email"]));
});

module.exports = router;
